#!/bin/bash
clear
echo "**************gateway monitor  installer****************";
# Stop on the first sign of trouble
set -e

if [ $UID != 0 ]; then
    echo "ERROR: Operation not permitted. Forgot sudo?"
    exit 1
fi

SCRIPT_DIR=$(pwd)

echo "Asia/Kolkata" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata
python3 -m pip install -r dependancy.txt
cp -Rf gatewaystatus /boot/lorainabox-2.0/lora
cp gatewaystatus/gatewaystatus.service /lib/systemd/system
cp checkservice /usr/bin/
chmod +x /usr/bin/checkservice
cp checkservice.service /lib/systemd/system

systemctl enable gatewaystatus.service

systemctl start gatewaystatus.service
systemctl enable checkservice.service

systemctl start  checkservice.service


echo "**************finish installation ****************";

